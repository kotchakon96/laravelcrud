@extends('layouts.master')

@section('content')
    <h1>Add New People</h1>
    <hr>
    <form action="{{ url('people') }}" method="post">
        @csrf
        <div class="form-group">
            <label>Firstname</label>
            <input class="form-control" type="text" name="fname">
        </div>
        <div class="form-group">
            <label>Lastname</label>
            <input class="form-control" type="text" name="lname">
        </div>
        <div class="form-group">
            <label>Age</label>
            <input class="form-control" type="text" name="age">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $errors)
                        <li>{{ $errors }}</li>
                    @endforeach
                </ul>
            </div>  
        @endif
        <button type="submit" class="btn btn-success">Save</button> 
    </form>    
@endsection