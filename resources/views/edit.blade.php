@extends('layouts.master')

@section('content')
    <h1>Edit People</h1>
    <hr>
    <form action="{{ url('people', [$people->id]) }}" method="post">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Firstname</label>
            <input class="form-control" type="text" name="fname" value="{{ $people->fname }}">
        </div>
        <div class="form-group">
            <label>Lastname</label>
            <input class="form-control" type="text" name="lname" value="{{ $people->lname }}">
        </div>
        <div class="form-group">
            <label>Age</label>
            <input class="form-control" type="text" name="age" value="{{ $people->age }}">
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $errors)
                        <li>{{ $errors }}</li>
                    @endforeach
                </ul>
            </div>  
        @endif
        <button type="submit" class="btn btn-success">Edit</button>
    </form>    
@endsection