@extends('layouts.master')
@section('title', 'List')
@section('css')
    @parent
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
@endsection

@section('content')
    @if (Session::has('message'))
        <div class="alert alert-success">
            {{ Session::get('message') }}
        </div>
    @endif
<div class="container mt-4">
    <div class="col-md-12 text-md-right">
        <a class="btn btn-success" href="add" role="button">Add</a>
    </div>
    <hr>
    <table class="table">
        <thead class="thead-dark">
        <tr align="center">
            <th>ID</th>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Age</th>
            <th>Time Create</th>
            <th>Time Update</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($people as $p)
                <tr align="center">
                    <td>{{ $p->id}}</td>
                    <td>{{ $p->fname}}</td>
                    <td>{{ $p->lname}}</td>
                    <td>{{ $p->age}}</td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($p->created_at)) }}</td>
                    <td>{{ date('d/m/Y H:i:s', strtotime($p->updated_at)) }}</td>
                    <td><a class="btn btn-warning" href="{{ url('people/' . $p->id . '/edit') }}" role="button">Edit</a></td>
                    <td>
                        <form action="{{ url('people', [$p->id]) }}" method="post">
                            @csrf
                            @method('delete')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr> 
            @endforeach
        </tbody>
    </table>
</div>
@endsection

@section('js')
    <script>
    
    </script>
@endsection